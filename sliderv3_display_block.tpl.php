<?php
global $base_url;

$image_style = check_plain($slider_data['slider_style']);
$slider_mac = check_plain($slider_data['machine_name']);
$disable_ticker = (int)$slider_data['slider_disable_ticker'];


?>

<div id="<?php echo $slider_mac; ?>" class="owl-carousel">

<?php foreach ($slider_images as $i => $value){
  

    $anchor = (trim($value['anchor'])!="")?$value['anchor']:"javascript:;";
    $target = ((int)$value['atarget']==1)?"_blank":"";
    
    $file = file_load($value['fid']);
    
    echo "<div class=\"item\">" .
    l(
          "<img title='{$value['ftitle']}' src='" . image_style_url($image_style, ($file->uri)) . "' />", $anchor, array(
            'attributes' =>
            array(
              'target' => $target,
              'class' => '',
            ),
            'html' => TRUE,
            'fragment' => '',
            'external'=>true   //allow blank/fractal target URL's
        )
    );
    if ($disable_ticker == 0) {
        echo "<div class=\"ticker-tx\">" .
        $value['ticker_text'] .
        "</div>";
    }
    echo "</div>";
   
}
//    exit;
?>
    
</div>

<?php 
    $singleitem = ((int)$slider_data['slider_item_count'] > 1) ? 'false' : 'true';
    $disable_nav = ((int)$slider_data['slider_disable_nav'] == 0)? 'true':'false';

    $jquery = "navigation   : {$disable_nav}, // Show next and prev buttons
            pagination      : true, 
            slideSpeed      : {$slider_data['slider_item_duration']},
            paginationSpeed : {$slider_data['slider_item_duration']},
            autoPlay        : {$slider_data['slider_autoplay_duration']}, //Set AutoPlay to 3 seconds
            

            singleItem      : {$singleitem},
            items           : {$slider_data['slider_item_count']}";

drupal_add_js('
    jQuery(document).ready(function($) {
     
        jQuery("#'.$slider_mac.'").owlCarousel({
            '.$jquery.'
        });
     
    });

','inline');


drupal_add_css(" 

    #{$slider_mac} .owl-carousel.owl-theme{
        background-color: {$slider_data['slider_bg_color']};
        
    }
    #{$slider_mac} .item img{
        display: block;
        width: 100%;
        height: auto;
        margin: auto;
    }
    
    .ticker-tx {
        text-align: center;
    }
    #{$slider_mac} .owl-carousel{
        width: {$slider_data['slider_image_width']}%;
    }

",'inline');
    
?>